pub fn enum_basics() {
    println!("\n-- fruitful enums --");

    #[derive(Debug)]
    enum Fruit {
        Apple,
        Pear,
    }

    let _a: Fruit = Fruit::Apple;
    let _b = Fruit::Apple;

    fn press(fruit: Fruit) {
        println!("got a {fruit:?}");
    }

    press(Fruit::Pear);
}

pub fn enum_data() {
    println!("\n-- enums with associated data --");

    #[derive(Debug)]
    enum Word {
        Noun(String),
        CompoundNoun(String, String),
        Adjective(String),
        Verb(String),
    }

    let a = Word::Noun(String::from("bicycle"));
    let b = Word::Adjective(String::from("green"));
    let c = Word::Verb(String::from("jump"));
    let d = Word::CompoundNoun(String::from("alter"), String::from("ego"));

    println!("{a:?} {b:?} {c:?} {d:?}");
}

pub fn enum_mixed() {
    println!("\n-- enums with mixed kinds of data --");

    #[derive(Debug)]
    #[allow(dead_code)]
    enum Fun {
        Foo,
        Bar(String, u32),
        Baz {x: u32, y: u32},
    }

    let f1: Fun = Fun::Foo;
    let f2 = Fun::Bar(String::from("well"), 456);
    let f3 = Fun::Baz {x: 1, y: 2};
    println!("{f1:?} {f2:?} {f3:?}");
    // ToDo: find out how to get f2.x and f2.y (or see enum_method() below)
    // warning: fields `x` and `y` are never read
}

pub fn enum_method() {
    println!("\n-- enums with methods --");

    enum Something {
        A(u32),
        B(u32),
        C {x: u32, y: u32},
    }

    impl Something {
        fn value(&self) -> u32 {
            match self {
                Something::A(x) => x.clone(),
                Something::B(x) => x.clone(),
                Something::C {x, y} => x + y,
            }
        }

        fn double(&self) -> u32 {
            self.value() * 2
        }
    }

    let a = Something::A(123);
    let b = Something::B(456);
    let c = Something::C {x: 3, y: 4};

    for x in [a, b, c] {
        println!("{} {}", x.value(), x.double());
    }
}

pub fn enum_option() {
    println!("\n-- Option enum examples --");

    fn add_or_not(x: Option<i32>, y: i32) -> Option<i32> {
        match x {
            None => None,
            Some(x) => Some(x + y),
        }
    }

    let a = add_or_not(None, 3);
    println!("None + 3 = {a:?}");
    let b = add_or_not(Some(4), 7);
    println!("4 + 7 = {b:?}");
}

pub fn enum_other() {
    println!("\n-- enum with match arm for other values --");

    #[derive(Debug)]
    #[allow(dead_code)]
    enum Blah {
        Foo,
        Bar,
        Baz,
        Bingo,
    }

    fn thingy(hey: &Blah) -> i32 {
        match hey {
            Blah::Foo => 1,
            _ => 3,
        }
    }

    let x = Blah::Foo;
    let y = thingy(&x);
    println!("x: {x:?}, y: {y:?}");

    let x = Blah::Bingo;
    let y = thingy(&x);
    println!("x: {x:?}, y: {y:?}");

    fn add_maybe(x: i32, y: i32) -> i32 {
        match x {
            1 => x,
            2 => y,
            other => other + y,
        }
    }

    let x = add_maybe(1, 2);
    let y = add_maybe(2, 3);
    let z = add_maybe(3, 4);
    println!("(1, 2) -> {x}, (2, 3) -> {y}, (3, 4) -> {z}");
}

pub fn enum_if_let() {
    println!("\n-- enum with if-let rather than match --");

    fn foo(x: Option<i32>) {
        match x {
            Some(value) => println!("Value: {value}"),
            _ => ()
        }
    }

    foo(Some(123));
    foo(None);
    foo(Some(456));

    fn bar(x: Option<i32>) {
        if let Some(value) = x {
            println!("Value: {value}");
        }
    }

    bar(Some(789));
    bar(None);
    bar(Some(234));

    fn baz(x: Option<i32>) {
        if let Some(value) = x {
            println!("Value: {value}");
        } else {
            println!("Value is None");
        }
    }

    baz(Some(432));
    baz(None);
    baz(Some(765));
}
