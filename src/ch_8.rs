pub fn collections_vectors() {
    println!("\n-- vectors --");

    let v1 = vec![4, 5, 6];
    println!("{}", v1[1]);

    let mut v2: Vec<f32> = Vec::new();
    v2.push(3.45);
    println!("{}", v2[0]);

    let x = v1[2];
    let y: &i32 = &v1[2];
    println!("{x} {y}");

    let z: Option<&i32> = v1.get(0);
    match z {
        Some(zval) => println!("{zval}"),
        None => println!("well..."),
    }
}
