pub fn struct_example() {
    println!("\n -- struct example --");

    struct Something {
        a: u32,
        b: String,
    }

    let s1 = Something {
        a: 123,
        b: String::from("Hello"),
    };

    println!("[{} {}]", s1.a, s1.b.as_str());

    let mut s2 = Something {
        a: 456,
        b: String::from("Bye-bye"),
    };

    println!("[{} {}]", s2.a, s2.b.as_str());
    s2.a = 457;
    println!("[{} {}]", s2.a, s2.b.as_str());
}

pub fn struct_tricks() {
    println!("\n -- struct tricks --");

    struct Something {
        a: u32,
        b: String,
        c: i64,
    }

    fn make_something(a: u32, b: &str, c: i64) -> Something {
        Something {
            a,  // Field init shorthand
            b: String::from(b),
            c,
        }
    }

    let s1 = make_something(789, "Nice", -5);

    println!("[{} {} {}]", s1.a, s1.b.as_str(), s1.c);

    let s2 = Something {
        c: -124,
        ..s1  // move all other fields from s1
    };

    println!("[{} {} {}]", s2.a, s2.b.as_str(), s2.c);

    // This wouldn't build as s1.b has been moved to s2
    // println!("{}", s1.b.as_str());
}

pub fn tuple_struct() {
    // Note: This is similar to the concept of "named tuples" in Python

    println!("\n --- tuple struct ---");

    struct Bingo(f32, f32, f32);

    let hey = Bingo(1.2, 3.4, 5.6);

    println!("{} {} {}", hey.0, hey.1, hey.2);

    struct Hertz();  // Unit struct

    let _freq = Hertz();

    println!("Size of Hertz: {}", std::mem::size_of::<Hertz>());
}

pub fn struct_display() {
    println!("\n --- struct display trait ---");

    struct Thing {
        rain: u32,
        bow: f64,
    }

    let thingy = Thing {
        rain: 123,
        bow: 2.0 / 3.0,
    };

    println!("thingy: {} {}", thingy.rain, thingy.bow);

    // now with Debug trait

    #[derive(Debug)]
    #[allow(dead_code)]
    struct ThingAgain {
        rain: u64,
        bow: f32,
    }

    let thingy = ThingAgain {
        rain: 123,
        bow: 2.0 / 3.0,
    };

    println!("thingy debug: {:?}", thingy);
    println!("thingy pretty: {:#?}", thingy);

    println!("Using the dbg macro for thingy2:");
    let thingy2 = ThingAgain {
        rain: dbg!(123 * 3),
        bow: 456.0,
    };
    println!("thingy2: {:?}", thingy2);
}

pub fn struct_impl() {
    println!("\n--- struct method implementation ---");

    #[derive(Debug)]
    struct Thing {
        rain: f32,
        bow: f32,
    }

    impl Thing {
        fn product(&self) -> f32 {
            self.rain * self.bow
        }

        fn equals(&self, other: &Thing) -> bool {
            self.rain == other.rain && self.bow == other.bow
        }
    }

    let thing = Thing {
        rain: 2.5,
        bow: 2.6,
    };

    println!("thing: {:?}", thing);
    println!("product: {}", thing.product());

    let other = Thing {
        rain: 2.5,
        bow: 2.6,
    };
    println!("equals {:?}: {}", other, thing.equals(&other));

    let other = Thing {
        rain: 2.5,
        bow: 2.61,
    };
    println!("equals {:?}: {}", other, thing.equals(&other));

    impl Thing {
        // Associated function (like a class method)
        fn magic() -> Self {
            Thing {
                rain: 7.8,
                bow: 9.0,
            }
        }

        fn pointless() -> u32 {
            123
        }
    }

    let hey = Thing::magic();
    println!("Thing from associated function: {hey:?}");
    let ho = Thing::pointless();
    println!("And an arbitrary associated function: {ho}");
}
