use std::cmp::Ordering;

pub fn built_in_types() {
    /* constants */

    println!("\n-- constants --");

    const A: f32 = 16.0;
    println!("A \t{A}");

    const B: f32 = 3.0;
    println!("B \t{B}");

    /* basic types */

    println!("\n-- basic types --");

    let x: u8 = (A / B) as u8;
    println!("u8 \t{x}");

    /*  This would cause an integer overflow panic at runtime:
    let mut x = 253u8;
    x += 9;
    */

    let x: f32 = A / B;  // x is "shadowing" the previous `let` statement
    println!("f32 \t{x}");

    let x: f64 = (A as f64) / (B as f64);
    println!("f64 \t{x}");

    let x: bool = A == B;
    println!("A == B \t{x}");
    let x: bool = A == A;
    println!("A == A \t{x}");

    let x = "Voilà.";
    println!("utf8 \t{x}");

    /* match */

    println!("\n-- match --");

    let x = match (A as u32).cmp(&(B as u32)) {
        Ordering::Greater => "gt",
        Ordering::Less => "lt",
        Ordering::Equal => "eq",
    };
    println!("cmp AB \t{x}");

    /* compound types */

    println!("\n-- compound types --");

    let x = (A, B, "Fun.");
    println!("x.0 \t{}", x.0);
    println!("x.1 \t{}", x.1);
    println!("x.2 \t{}", x.2);

    let (y, z) = (A, B);
    println!("y, z \t{y}, {z}");

    let x = [A, B, A / B];
    println!("x[2] \t{}", x[2]);

    const A64: f64 = A as f64;
    const B64: f64 = B as f64;
    let x: [f64; 3] = [A64, B64, A64 / B64];
    println!("x[2] \t{}", x[2]);
}

pub fn some_arguments(x: f32, y: f32) -> f64 {
    (x as f64) / (y as f64)
}

pub fn conditional() {
    println!("\n-- control flow --");

    const A: u32 = 123;
    const B: u32 = 456;

    if A > B {
        println!("A is greater than B");
    } else {
        println!("B is greater than or equal to A");
    }

    if A != 0 {
        println!("A is not 0");
    }

    let x = if A > B { "gt" }  else { "le" };
    println!("A is {x} B");
}

pub fn loops() {
    println!("\n-- loops --");
    let mut y = 123;

    let x = loop {
        let rem = y % 45;

        if rem == 0 {
            break (y, y / 45);
        }

        y -= 1;
    };
    println!("x: {}, {}", x.0, x.1);

    y = 123;
    let x = 'silly_loop: loop {
        loop {
            if y % 76 == 0 {
                break 'silly_loop y;
            }

            if y % 47 == 0 {
                print!("{y} ");
            };

            y -= 1;
        };
    };
    println!("x: {x}");

    let mut x = 789;
    while (x % 46) != 0 {
        x -= 1;
    };
    println!("x: {x}");

    let x = [1, 2, 3];
    print!("array:");
    for y in x.iter() {
        print!(" {y}");
    }
    print!(" |");
    for y in x {
        print!(" {y}");
    }
    println!();

    print!("range:");
    for x in 1..4 {
        print!(" {x}");
    }
    println!();
}

pub fn ownership() {
    println!("\n-- ownership --");

    let x = 123;
    let y = x;
    println!("x: {x}, y: {y}");

    // constant string on the stack
    let a = "Hello";
    let b = a;
    println!("a: {a}, b: {b}");

    // mutable string on the heap
    let c = String::from("Bye");
    let d = c;
    println!("d: {d}");  // c has now been "moved" to d, so c is invalidated
    let e = d.clone();
    println!("d: {d}, e: {e}");

    fn hey(blah: String) {
        println!("hey: {blah}");
    }

    hey(d);  // d gets "moved" to the hey() function so it's invalidated too
    println!("e: {e}");  // e is a clone of d, copied on the heap

    fn bingo(blah: String) -> String {
        println!("bingo: {blah}");
        blah
    }

    let f = bingo(e);  // e gets "moved" to the function but then back again
    println!("f: {f}");
}

pub fn references() {
    println!("\n-- references --");

    fn strlen(s: &String) -> usize {
        s.len()
    }

    let s = String::from("Alright.");
    let len = strlen(&s);  // s is "borrowed" b strlen() but not moved
    println!("s: {s}, len: {len}");

    fn add_xyz(s: &mut String) {
        s.push_str(" xyz");
    }

    let mut z = String::from("uvw");
    add_xyz(&mut z);
    println!("z: {z}");

    let dang = &z;
    print!("dang: {dang}");

    let y = &mut z;
    // can't do this because both y and z are used and there can be only one
    // current mutable reference:
    // println!("y: {y}, z: {z}");
    // can't do this here either because dang is immutable but y is mutable:
    // print!("dang: {dang}");
    // but this is OK:
    print!("y: {y}");
    println!(", z: {z}");
    // and now y is not valid, so this won't work: println!("y: {y}");
    // this does work though as the original z is still here:
    let x = &mut z;
    println!("x: {x}");
    // this still works too with an immutable reference:
    let x = &z;
    println!("x: {x}, z: {z}");
}

pub fn slices() {
    println!("\n-- slices --");

    let strings: [&String; 3] = [
        &String::from("xyz"),
        &String::from("yxzx"),
        &String::from("abc def xxx"),
    ];

    fn count_x(s: &String) -> usize {
        let mut n: usize = 0;

        for c in s.chars() {
            if c == 'x' {
                n += 1;
            }
        }

        n
    }

    for s in strings.iter() {
        let n = count_x(s);
        println!("{}: {}", s, n);
    }

    fn sub_x(s: &str) -> &str {
        for (i, c) in s.chars().enumerate() {
            if c == 'x' {
                return &s[0..i];
            }
        }

        s
    }

    for s in strings.iter() {
        let sub = sub_x(s);
        println!("{}: [{}]", s, sub);
    }

    let mut s: String = String::from("now then");
    println!("{}, {}", s, &s[2..6]);
    let x: &str = &s[2..6];
    // This would fail to compile because of the immutable slice x
    // s.clear();
    println!("{}, {}", s, x);
    s.clear(); // now OK because x is not used later in the code
    // This would cause a panic at runtime:
    //println!("{}, {}", s, &s[2..6]);

    let z: [i32; 4] = [123, -45, 67, 89];
    for (i, value) in z.iter().enumerate() {
        println!("z[{i}]: {value}");
    }

    let w: &[i32] = &z[1..3];
    for (i, value) in w.iter().enumerate() {
        println!("w[{i}]: {value}");
    }
}

pub fn errors() {
    println!("\n-- errors --");

    fn str2int(str_value: &str) -> i32 {
        let x: i32 = match str_value.trim().parse() {
            Ok(value) => value,
            Err(_) => -1,
        };
        x
    }

    let values = ["123", "345", "abc", "12;34", "0x10", "-42", "123.45"];

    for x in values {
        let y: i32 = str2int(x);
        println!("{x} -> {y}");
    }
}

pub fn str_parse() {
    println!("\n-- str parse --");

    let x: u32 = "123".parse().expect("Oops");
    println!("value: {x}");

    let x: u32 = u32::from_str_radix("12", 16).expect("Oops");
    println!("value: {x}");

    let x: f32 = "123.45".parse().expect("Oops");
    println!("value: {x}");

    let x: f64 = "123.456789".parse().expect("Oops");
    println!("value: {x}");
}

pub fn wrap() {
    println!("\n-- wrap --");

    println!("u8:");
    let x: u8 = 253;
    for i in 1..5 {
        let y: u8 = x.wrapping_add(i);
        println!("{y}");
    }

    println!("i8:");
    let x: i8 = 125;
    for i in 1..5 {
        let y: i8 = x.wrapping_add(i);
        println!("{y}");
    }
}

pub fn tuple() {
    println!("\n-- tuple --");

    let x: (f32, &str, i8, String) = (123.45, "abcd", -7, String::from("hey"));
    println!("{} {} {} {}", x.3, x.2, x.1, x.0);
    let (a, b, c, d) = x;
    println!("[{a}] [{b}] [{c}] [{d}]");
    let y: &String = &d;
    println!("{y}");
}

pub fn array() {
    println!("\n-- array --");

    let x: [i16; 3] = [567, 789, -890];
    println!("{} {} {}", x[0], x[1], x[2]);

    let mut y: [i16; 3] = [567, 789, -890];
    y[0] = -123;
    println!("{} {} {}", y[0], y[1], y[2]);

    let z: [i8; 4] = [3; 4];
    println!("{} {}", z[0], z[3]);
}
