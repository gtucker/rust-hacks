use std::collections::HashMap;
use std::io::{self, Write};
mod ch_1_4;
mod ch_5;
mod ch_6;
mod ch_7;
mod submod;
mod ch_8;

fn run_ch_1_4() -> () {
    // Chapters 1 to 4
    ch_1_4::built_in_types();
    println!("\n-- function with arguments --");
    let res: f64 = ch_1_4::some_arguments(1.2, 3.4);
    println!("f64 result: 1.2 / 3.4 = {res}");
    ch_1_4::conditional();
    ch_1_4::loops();
    ch_1_4::ownership();
    ch_1_4::references();
    ch_1_4::slices();
    ch_1_4::errors();
    ch_1_4::str_parse();
    ch_1_4::wrap();
    ch_1_4::tuple();
    ch_1_4::array();
}

fn run_ch_5() -> () {
    ch_5::struct_example();
    ch_5::struct_tricks();
    ch_5::tuple_struct();
    ch_5::struct_display();
    ch_5::struct_impl();
}

fn run_ch_6() {
    ch_6::enum_basics();
    ch_6::enum_data();
    ch_6::enum_mixed();
    ch_6::enum_method();
    ch_6::enum_option();
    ch_6::enum_other();
    ch_6::enum_if_let();
}

fn run_ch_7() {
    ch_7::modules();
    ch_7::modules_super();
    ch_7::modules_files();
}

fn run_ch_8() {
    ch_8::collections_vectors();
}

fn run_input() {
    print!("Input: ");
    io::stdout().flush().unwrap();

    let mut raw_input = String::new();
    io::stdin()
        .read_line(&mut raw_input)
        .expect("Dang");

    let input_value = raw_input.trim();
    println!("Input value: {input_value}")
}

fn unknown_command(cmd: &str) -> () {
    println!("Unknown command: {cmd}");
}

fn main() {
    let mut cmd_map: HashMap<&str, fn() -> ()> = HashMap::new();

    cmd_map.insert("ch1-4", run_ch_1_4);
    cmd_map.insert("ch5", run_ch_5);
    cmd_map.insert("ch6", run_ch_6);
    cmd_map.insert("ch7", run_ch_7);
    cmd_map.insert("ch8", run_ch_8);
    cmd_map.insert("input", run_input);

    let cmd: String = match std::env::args().nth(1) {
        None => String::from(""),
        Some(x) => x,
    };

    if cmd != String::from("") {
        let cmd_str = cmd.as_str();
        match cmd_map.get(cmd_str) {
            None => unknown_command(cmd_str),
            Some(cmd_fn) => cmd_fn(),
        };
    } else {
        println!("Available commands:");
        for cmd_str in cmd_map.keys() {
            println!("* {}", cmd_str);
        }
    }
}
