use crate::submod::hello;

mod thingy {
    pub fn thing() -> String {
        String::from("something")
    }

    pub fn foo() -> i32 {
        return 123
    }
}

use thingy::foo;

mod bingo {
    pub fn bang() {
        println!("Bang");
    }

    pub fn boom() {
        println!("Boom");
    }

    pub fn pow() {
        println!("Pow");
    }
}

pub fn modules() {
    println!("\n-- using local sub-modules --");

    println!("Hello");
    let x: String = thingy::thing();
    println!("thing: {x}");
    let y = foo();
    println!("foo: {y}");
    crate::ch_7::bingo::bang();
    self::bingo::boom();
    bingo::pow();
}

mod here {
    pub fn parent() {
        println!("here::parent()");
    }

    pub mod there {
        pub fn call_parent() {
            super::parent();
        }
    }
}

pub fn modules_super() {
    println!("\n-- using the parent module --");

    self::here::there::call_parent();
}

pub fn modules_files() {
    println!("\n-- using a module in a file --");

    let msg = hello::hey();
    println!("Hey message: {msg}");
}
